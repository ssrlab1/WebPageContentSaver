<?php
	class WebPageContentSaver {
		private static $localizationArr = array();
		private static $localizationErr = array();
		private $url = '';
		private $result_text_full = '';
		private $result_text_clear = '';		
		private $result_url_full = '';
		private $result_url_clear = '';
		const BR = "<br>\n";
		
		function __construct() {
			
		}
		
		public static function loadLanguages() {
			$languages = array();
			$files = scandir('lang/');
			if(!empty($files)) {
				foreach($files as $file) {
					if(substr($file, 2, 4) == '.txt') {
						$languages[] = substr($file, 0, 2);
					}
				}
			}
			return $languages;
		}
		
		public static function loadLocalization($lang) {
			$filepath = "lang/$lang.txt";
			$linesArr = file($filepath, FILE_IGNORE_NEW_LINES);
			if($linesArr === false) {
				$filepath = "lang/en.txt";
				$linesArr = file($filepath, FILE_IGNORE_NEW_LINES);
			}
			foreach($linesArr as $line) {
				if(empty($line)) {
					$key = $value = '';
				}
				elseif(substr($line, 0, 1) !== '#' && substr($line, 0, 2) !== '//') {
					if(empty($key)) {
						$key = $line;
					}
					else {
						if(!isset(self::$localizationArr[$key])) {
							self::$localizationArr[$key] = $line;
						}
					}
				}
			}
		}
		
		public static function showMessage($msg) {
			if(isset(self::$localizationArr[$msg])) {
				return self::$localizationArr[$msg];
			}
			else {
				self::$localizationErr[] = $msg;
				return $msg;
			}
		}
		
		public static function sendErrorList($lang) {
			if(!empty(self::$localizationErr)) {
				$ip = str_replace('.', '-', $_SERVER['REMOTE_ADDR']);
				$sendersName = 'Web Page Content Saver';
				$recipient = 'corpus.by@gmail.com';
				$subject = "ERROR: Incorrect localization in $sendersName by user $ip";
				$mailBody = 'Вітаю, гэта corpus.by!' . self::BR;
				$mailBody .= $_SERVER['HTTP_HOST'] . '/WebPageContentSaver/ дасылае інфармацыю аб наступных памылках:' . self::BR . self::BR;
				$mailBody .= "Мова лакалізацыі: <b>$lang</b>" . self::BR;
				$mailBody .= 'Адсутнічае лакалізацыя наступных ідэнтыфікатараў:' . self::BR . implode(self::BR, self::$localizationErr) . self::BR;
				$header  = "MIME-Version: 1.0\r\n";
				$header .= "Content-type: text/html; charset=utf-8\r\n";
				$header .= "From: $sendersName <corpus.by@gmail.com>\r\n";
				mail($recipient, $subject, $mailBody, $header);
			}
		}
		
		private function validateURL($url) {
			$pattern = "/^(?:(?:https?):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]$/i";
			if(preg_match($pattern, $url)) {
				return true;
			} else {
				return false;
			}
    	}

		public function setText($url) {
			$url = trim($url);
			if($this->validateURL($url)) {
				$this->url = $url;
				return true;
			}
			return false;
		}
		
		public function run() {
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $this->url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_FAILONERROR, true);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_AUTOREFERER, 1); 
			curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0');  
			$this->result_text_full = curl_exec($ch);
			curl_close($ch);
			$this->result_text_clear = $this->html2txt($this->result_text_full);
		}
		
		private function html2txt($text) {
			$search = array('@<script[^>]*?>.*?<\/script>@smi',  // Strip out javascript
							'@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
							'@<style[^>]*?>.*?<\/style>@smiU',    // Strip style tags properly
							'@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments including CDATA
			);
			$text = preg_replace($search, '', $text);
			$text = preg_replace('/\xc2\xa0/', ' ', $text);
			$text = preg_replace('/\s+/', ' ', $text); // remove all multiple whitespaces
			$text = trim($text);
			return $text;
		}
		
		public function saveCacheFiles() {
			mb_internal_encoding('UTF-8');
			mb_regex_encoding('UTF-8');
			
			$date_code = date('Y-m-d_H-i-s', time());
			$rand_code = rand(0, 1000);
			$ip = str_replace('.', '-', $_SERVER['REMOTE_ADDR']);
			
			$root = $_SERVER['HTTP_HOST'];
			$serviceName = 'WebPageContentSaver';
			$sendersName = 'Web Page Content Saver';
			$senders_email = "corpus.by@gmail.com";
			$recipient = "corpus.by@gmail.com";
			$subject = "Getting page by URL from IP $ip";
			$mail_body = "Вітаю, гэта corpus.by!" . self::BR;
			$mail_body .= "$root/$serviceName/ дасылае інфармацыю аб актыўнасці карыстальніка з IP $ip." . self::BR . self::BR;
			
			$cachePath = dirname(dirname(__FILE__)) . "/_cache";
			if(!file_exists($cachePath)) mkdir($cachePath);
			$cachePath = "$cachePath/$serviceName";
			if(!file_exists($cachePath)) mkdir($cachePath);
			
			$filename = $date_code . '_'. $ip . '_' . $rand_code . '_url.txt';
			$path = "$cachePath/in/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$new_file = fopen($filepath, 'wb') OR die('open cache file error');
			fwrite($new_file, $this->url);
			fclose($new_file);
			$mail_body .= "URL: " . $this->url . self::BR . self::BR;
			$mail_body .= "URL захаваны тут: " . dirname(dirname(__FILE__)) . "/showCache.php?s=WebPageContentSaver&t=in&f=$filename" . self::BR . self::BR;
			
			$filename = $date_code . '_'. $ip . '_' . $rand_code . '_full_page.txt';
			$path = "$cachePath/out/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$new_file = fopen($filepath, 'wb') OR die('open cache file error');
			$cache_text = $this->result_text_full;
			fwrite($new_file, $cache_text);
			fclose($new_file);
			$mail_body .= "Выніковы тэкст цалкам захаваны тут: " . dirname(dirname(__FILE__)) . "/showCache.php?s=WebPageContentSaver&t=out&f=$filename" . self::BR . self::BR;		
			$this->result_url_full = "../_cache/$serviceName/out/$filename";
			
			$filename = $date_code . '_'. $ip . '_' . $rand_code . '_clear_page.txt';
			$path = "$cachePath/out/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$new_file = fopen($filepath, 'wb') OR die('open cache file error');
			$cache_text = $this->result_text_clear;
			fwrite($new_file, $cache_text);
			fclose($new_file);
			$mail_body .= "Выніковы тэкст без тэгаў цалкам захаваны тут: " . dirname(dirname(__FILE__)) . "/showCache.php?s=WebPageContentSaver&t=out&f=$filename" . self::BR . self::BR;		
			$this->result_url_clear = "../_cache/$serviceName/out/$filename";
			
			$header  = "MIME-Version: 1.0\r\n";
			$header .= "Content-type: text/html; charset=utf-8\r\n";
			$header .= "From: $sendersName <$senders_email>\r\n";
			mail($recipient, $subject, $mail_body, $header);
			
			$filename = $date_code . '_' . $ip . '_' . $rand_code . '_e.txt';
			$path = "$cachePath/email/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$newFile = fopen($filepath, 'wb') OR die('open cache file error');
			fwrite($newFile, join("\n", array("recipient: \t" . $recipient, "subject: \t" . $subject, "\n\tMAIL BODY\n\n" . $mail_body, $header)));
			fclose($newFile);
		}
		
		public function getResultUrlFull() {
			return $this->result_url_full;
		}
		
		public function getResultUrlClear() {
			return $this->result_url_clear;
		}
	}
?>
