<?php
	header("Content-type: text/html;  charset=utf-8");
	header("Access-Control-Allow-Origin: *");
	mb_internal_encoding('UTF-8');
	mb_regex_encoding('UTF-8');
		
	$localization = isset($_POST['localization']) ? $_POST['localization'] : 'en';
	$url = isset($_POST['url']) ? $_POST['url'] : '';
	
	include_once 'WebPageContentSaver.php';
	WebPageContentSaver::loadLocalization($localization);
	
	$msg = '';
	if(!empty($url)) {
		$WebPageContentSaver = new WebPageContentSaver();		
		if($WebPageContentSaver->setText($url)){
			$WebPageContentSaver->run();
			$WebPageContentSaver->saveCacheFiles();

			$result['url'] = $url;
			$result['full'] = $WebPageContentSaver->getResultUrlFull();
			$result['clear'] = $WebPageContentSaver->getResultUrlClear();
			$msg = json_encode($result);
		}
		else {
			$result['url'] = $url;
			$result['error'] = 'URL is not correct';
			$msg = json_encode($result);
		}
		
	}
	echo $msg;
?>
