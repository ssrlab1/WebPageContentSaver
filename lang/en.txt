title
Web Page Content Saver

help
https://ssrlab.by/en/3214

be
Беларуская

ru
Русский

en
English

input
Please, enter a link to a web page

button
Get content of the web page!

result
Result

result1
Full result page:

download1
Download full html page

result2
Result page without tags:

download2
Download html page without tags

service code
Service source code can be downloaded

reference
here

suggestions
Suggest an improvement of the service

contact e-mail
We would be glad to receive your feedback and suggestions via e-mail

other prototypes
Our other prototypes:

laboratory
Speech synthesis and recognition laboratory, UIIP NAS Belarus
